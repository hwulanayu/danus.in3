package com.danusin.log_in_out;

import com.danusin.user_interface.UIApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = UIApplication.class)
public class UIApplicationTests {

	@Test
	public void contextLoads() {
	}

}
