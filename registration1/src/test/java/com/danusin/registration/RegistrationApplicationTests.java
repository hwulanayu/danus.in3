package com.danusin.registration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("test")
@SpringBootApplication
public class RegistrationApplicationTests {

    public static void main(String[] args){
        SpringApplication.run(RegistrationApplication.class, args);
    }

}
