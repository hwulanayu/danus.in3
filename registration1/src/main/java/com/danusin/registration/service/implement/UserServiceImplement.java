package com.danusin.registration.service.implement;

import com.danusin.registration.domain.persistence.User;
import com.danusin.registration.domain.repository.UserRepository;
import com.danusin.registration.service.UserService;
import com.danusin.registration.vo.AuthVo;
import com.danusin.registration.vo.ResponseVo;
import com.danusin.registration.vo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.transaction.Transactional;
import java.security.Key;
import java.util.Collection;

@Service
public class UserServiceImplement implements UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    @Transactional
    public User createUser(User user) {
        user.setRestAuth(user.getPassword());
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRepository.save(user);
        return user;
    }

    @Override
    @Transactional
    public ResponseVo createUser(UserVo vo) {
        if (null == vo)
            return null;
        if (null == vo.getUsername() || null == vo.getPassword() ||
        "".equals(vo.getUsername()) || "".equals(vo.getPassword()))
            return null;
        User user = new User();
        user.setUsername(vo.getUsername());
        user.setPassword(bCryptPasswordEncoder.encode(vo.getPassword()));
        user.setName(vo.getName());
        user.setRestAuth(vo.getPassword());
        user.setPhoneNumber(vo.getPhoneNumber());
        user.setCompany(vo.getCompany());
        user = userRepository.save(user);
        if (null == user)
            return null;
        if (null == user.getId())
            return null;
        ResponseVo responseVo = new ResponseVo();
        responseVo.setSuccess(true);
        return responseVo;
    }

    @Override
    public Collection<User> getListUsers() {
        return userRepository.findAll();
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public UserVo isAuth(String authString){
        if (null == authString)
            return null;
        String[] usersString = authString.split("===");
        if (usersString.length < 2)
            return null;
        User user = userRepository.findByUsername(usersString[0]);
        if (null == user)
            return null;
        if (!user.getPassword().equals(usersString[1]))
            return null;
        if (!user.isLoggedIn())
            return null;
        UserVo vo = new UserVo();
        vo.setUsername(user.getUsername());
        vo.setName(user.getName());
        vo.setCompany(user.getCompany());
        vo.setPhoneNumber(user.getPhoneNumber());
        return vo;
    }

    @Override
    public AuthVo credential(String username){
        if (null == username)
            return null;
        User user = userRepository.findByUsername(username);
        if (null == user)
            return null;
        AuthVo vo = new AuthVo();
        vo.setAuthString(user.getUsername().concat("===").concat(user.getPassword()));
        return vo;
    }

    @Override
    @Transactional
    public AuthVo restLogin(String username, String password) {
        if (null == username || null == password ||
                "".equals(username) || "".equals(password))
            return null;
        User user = userRepository.findByUsername(username);
        if (null == user)
            return null;
        if (!password.equals(user.getRestAuth()))
            return null;
        user.setLoggedIn(true);
        user = userRepository.save(user);
        if (null == user)
            return null;
        AuthVo vo = new AuthVo();
        vo.setAuthString(user.getUsername().concat("===").concat(user.getPassword()));
        return vo;
    }

    @Override
    @Transactional
    public ResponseVo restLogout(String authString) {
        if (null == authString || "".equals(authString))
            return null;
        String[] usersString = authString.split("===");
        if (usersString.length < 2)
            return null;
        User user = userRepository.findByUsername(usersString[0]);
        if (null == user)
            return null;
        if (!user.getPassword().equals(usersString[1]))
            return null;
        user.setLoggedIn(false);
        user = userRepository.save(user);
        if (null == user)
            return null;
        ResponseVo responseVo = new ResponseVo();
        responseVo.setSuccess(true);
        return responseVo;
    }
}
