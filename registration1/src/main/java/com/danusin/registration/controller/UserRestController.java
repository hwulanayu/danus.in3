package com.danusin.registration.controller;

import com.danusin.registration.domain.persistence.User;
import com.danusin.registration.service.UserService;
import com.danusin.registration.vo.AuthVo;
import com.danusin.registration.vo.ResponseVo;
import com.danusin.registration.vo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("/rest/user")
public class UserRestController {
    @Autowired
    UserService userService;

    // return user if it is authenticated
    @GetMapping(value = "/is-auth", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public UserVo isAuth(@RequestParam(value = "auth-string") String authString){
        return userService.isAuth(authString);
    }

    // return string for credential
    @GetMapping(value = "/credential", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public AuthVo credential(Principal principal){
        return userService.credential(principal.getName());
    }

    @PostMapping(value = "",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseVo addUser(@RequestBody UserVo vo){
        return userService.createUser(vo);
    }

    @PostMapping(value = "/rest-login",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public AuthVo restLogin(@RequestParam(value = "username") String username,
                            @RequestParam(value = "password") String password){
        return userService.restLogin(username, password);
    }

    @PostMapping(value = "/rest-logout",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseVo restLogout(@RequestParam(value = "auth-string") String authString){
        return userService.restLogout(authString);
    }
}
