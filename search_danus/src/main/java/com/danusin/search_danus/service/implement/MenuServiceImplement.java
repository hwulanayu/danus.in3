package com.danusin.search_danus.service.implement;

import com.danusin.search_danus.domain.persistence.MenuModel;
import com.danusin.search_danus.domain.repository.MenuDb;
import com.danusin.search_danus.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class MenuServiceImplement implements MenuService {

    @Autowired
    private MenuDb menuDb;

    @Override
    public MenuModel createMenu(MenuModel menuModel) {
        menuDb.save(menuModel);
        return menuModel;
    }

    @Override
    public Collection<MenuModel> getListMenu() {
        return menuDb.findAll();
    }

    @Override
    public MenuModel findByNamaMakanan(String namaMakanan) {
        return menuDb.findByNamaMakanan(namaMakanan);
    }
}
