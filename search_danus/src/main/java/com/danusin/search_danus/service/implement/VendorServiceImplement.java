package com.danusin.search_danus.service.implement;

import com.danusin.search_danus.domain.persistence.VendorModel;
import com.danusin.search_danus.domain.repository.VendorDb;
import com.danusin.search_danus.service.VendorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class VendorServiceImplement implements VendorService {

    @Autowired
    private VendorDb vendorDb;

    @Override
    public VendorModel createVendor(VendorModel vendorModel) {
        vendorDb.save(vendorModel);
        return vendorModel;
    }

    @Override
    public Collection<VendorModel> getListVendor() {
        return vendorDb.findAll();
    }

    @Override
    public VendorModel findByNamaVendor(String namaVendor) {
        return vendorDb.findByNamaVendor(namaVendor);
    }

}
