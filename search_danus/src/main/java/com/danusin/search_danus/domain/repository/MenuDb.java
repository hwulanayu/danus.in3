package com.danusin.search_danus.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.danusin.search_danus.domain.persistence.MenuModel;

@Repository
public interface MenuDb extends JpaRepository<MenuModel, Integer>{
    MenuModel findByNamaMakanan(String namaMakanan);
}